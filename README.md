# Emergency gate project (e-gate)

The idea is to distribute and show the way to assemble fast and low cost an effective sanitary gate.

# Projecto de túnel sanitario (e-gate)

Nuestra idea es distibuir y mostrar como ensamblar de forma rápida y de bajo costo un túnel sanitario efectivo.

## Licencia 📄

Este contenido está bajo licencia GNU General Public License (GNU GPL), los usuarios finales (personas, organizaciones, compañías) tienen la libertad de usar, estudiar, compartir (copiar) y modificar el contenido

##Construyendo los túneles sanitarios ⚙️
La idea es entregar un listado de materiales e instrucciones para ensamblar los sistemas de control, cableado y estructura de un túnel sanitario

##Construido con 🛠️

Los planos y documentos de este proyecto están construidos con los siguientes software

Google spreadsheet - Gestor web de hojas de datos
Sketchup 2018 - Modelador 3D de partes y piezas
V-Ray - Renderizador 3D
Layout 2018 - Para dibujar planos eléctricos