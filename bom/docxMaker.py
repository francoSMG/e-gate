import docx
from docx import Document
from docx.shared import Inches,Pt
import pandas as pd
from docx.enum.dml import MSO_THEME_COLOR_INDEX
from docx.enum.text import WD_ALIGN_PARAGRAPH

def add_hyperlink(paragraph, text, url):
    # This gets access to the document.xml.rels file and gets a new relation id value
    part = paragraph.part
    r_id = part.relate_to(url, docx.opc.constants.RELATIONSHIP_TYPE.HYPERLINK, is_external=True)

    # Create the w:hyperlink tag and add needed values
    hyperlink = docx.oxml.shared.OxmlElement('w:hyperlink')
    hyperlink.set(docx.oxml.shared.qn('r:id'), r_id, )

    # Create a w:r element and a new w:rPr element
    new_run = docx.oxml.shared.OxmlElement('w:r')
    rPr = docx.oxml.shared.OxmlElement('w:rPr')

    # Join all the xml elements together add add the required text to the w:r element
    new_run.append(rPr)
    new_run.text = text
    hyperlink.append(new_run)

    # Create a new Run object and add the hyperlink into it
    r = paragraph.add_run ()
    r._r.append (hyperlink)

    # A workaround for the lack of a hyperlink style (doesn't go purple after using the link)
    # Delete this if using a template that has the hyperlink style in it
    r.font.color.theme_color = MSO_THEME_COLOR_INDEX.HYPERLINK
    r.font.underline = True

    return hyperlink

materials = pd.read_excel('./bom.xlsx')


document = Document()
document.add_heading('BOM (Bill of materials)', 0)
document.add_heading('Listado de materiales Chile', 0)
paragraph_format = document.styles['Normal'].paragraph_format
document.add_paragraph('A continuación un listado de materiales para armar un e-gate usando materiales disponibles. Se dividen por categorías y tienen referencias a distintos proveedores. ')
#groupedMaterials=materials.groupby(['CATEGORÍA','ID_PLANOS'])
groupedCategory=materials.groupby(['CATEGORÍA'])
for category,data0 in groupedCategory:
    document.add_heading('Categoria : '+category, level=1)
    groupedByID_PLANOS=data0.groupby(['ID_PLANOS'])
    for id_planos,data1 in groupedByID_PLANOS:
        document.add_heading('ID planos : '+id_planos, level=2)
        groupedByDF=data1.groupby(['DESCRIPCIÓN','FUNCIÓN'])
        counterOpciones=1
        for DF,data2 in groupedByDF:
            document.add_heading('Opción : '+str(counterOpciones),level=3)
            document.add_paragraph('Descripción : '+DF[0]+'\n'+
                                   'Función : '+DF[0]+'\n'+
                                   'Costo total promedio : $'+str(data2['COSTO_UNITARIO_REFERENCIA'].mean())+'\n')         
            document.add_paragraph('Listado de proveedores ')
            counterProveedor=1
            table = document.add_table(rows=0, cols=2)
            for index,item in data2.iterrows():
                paragraph_format.space_after = Pt(0)
                row_cells = table.add_row().cells
                p=row_cells[0].add_paragraph(str(counterProveedor)+'. Proveedor - Modelo : '+item['PROVEEDOR-MODELO']+'\n'+
                                         'Cantidad necesaria / unidad : '+str(item['CANTIDAD'])+' / '+item['UNIDAD']+'\n'+
                                         'Costo unitario de referencia : $'+str(item['COSTO_UNITARIO_REFERENCIA'])+'  '+item['MONEDA']+'\n'+     
                                         'Referencia : ')        
                add_hyperlink(p, 'Link', item['REFERENCIA'])
                p=row_cells[1].add_paragraph('')
                r = p.add_run()
                r.add_picture('./img/'+str(item['ID'])+'.jpg',width=Inches(1))
                p.alignment=WD_ALIGN_PARAGRAPH.CENTER
                table.cell(0,0).width = Inches(5)
                document.add_paragraph(' ')
                counterProveedor+=1
            counterOpciones+=1
document.save('bom.docx')       




document.add_paragraph('Intense quote', style='Intense Quote')
document.add_paragraph(
    'first item in unordered list', style='List Bullet')
document.add_paragraph(
    'first item in ordered list', style='List Number')

document.add_picture('modelo3D-render-04.png', width=Inches(1.25))

records = (
    (3, '101', 'Spam'),
    (7, '422', 'Eggs'),
    (4, '631', 'Spam, spam, eggs, and spam')
)

table = document.add_table(rows=1, cols=3)
hdr_cells = table.rows[0].cells
hdr_cells[0].text = 'Qty'
hdr_cells[1].text = 'Id'
hdr_cells[2].text = 'Desc'
for qty, id, desc in records:
    row_cells = table.add_row().cells
    row_cells[0].text = str(qty)
    row_cells[1].text = id
    row_cells[2].text = desc

document.add_page_break()

